import os
import unittest

from termcolor_enum import *


class Test(unittest.TestCase):
    def setUp(self) -> None:
        print(f"\n<=< Current terminal type: {os.getenv('TERM')} >=>")

    def test_colors(self):
        cprint("Grey", Colors.GREY)
        cprint("Red", Colors.RED)
        cprint("Green", Colors.GREEN)
        cprint("Yellow", Colors.YELLOW)
        cprint("Blue", Colors.BLUE)
        cprint("Magenta", Colors.MAGENTA)
        cprint("Cyan", Colors.CYAN)
        cprint("White", Colors.WHITE)

    def test_highlights(self):
        cprint("On grey", on_color=Highlights.ON_GREY)
        cprint("On red", on_color=Highlights.ON_RED)
        cprint("On green", on_color=Highlights.ON_GREEN)
        cprint("On yellow", on_color=Highlights.ON_YELLOW)
        cprint("On blue", on_color=Highlights.ON_BLUE)
        cprint("On magenta", on_color=Highlights.ON_MAGENTA)
        cprint("On cyan", on_color=Highlights.ON_CYAN)
        cprint("On white", color=Colors.GREY, on_color=Highlights.ON_WHITE)

    def test_attributes(self):
        cprint("Bold grey", "grey", attrs=[Attributes.BOLD])
        cprint("Dark red", "red", attrs=[Attributes.DARK])
        cprint("Underline green", "green", attrs=[Attributes.UNDERLINE])
        cprint("Blink yellow", "yellow", attrs=[Attributes.BLINK])
        cprint("Reversed blue", "blue", attrs=[Attributes.REVERSE])
        cprint("Concealed magenta", "magenta", attrs=[Attributes.CONCEALED])
        cprint(
            "Bold underline reverse cyan",
            "cyan",
            attrs=[
                Attributes.BOLD,
                Attributes.UNDERLINE,
                Attributes.REVERSE,
            ],
        )
        cprint(
            "Dark blink concealed white",
            "white",
            attrs=[
                Attributes.DARK,
                Attributes.BLINK,
                Attributes.CONCEALED,
            ],
        )

    def test_mixing(self):
        cprint("Underline red on grey", Colors.RED, "on_grey", ["underline"])
        cprint("Reversed green on red", Colors.GREEN, "on_red", ["reverse"])
