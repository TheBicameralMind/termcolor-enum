__version__ = "1.2.0"

from .termcolor import cprint, colored
from .enums import Colors, Highlights, Attributes

__all__ = ["cprint", "colored", "Colors", "Highlights", "Attributes"]
